# Netlify Deploy Button

This is a simple module which takes webhook URL and will trigger the 
deployment on Netlify.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/ndb).
Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/ndb).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Enable the module
1. You can find the menu under the Configuration tab named 'Deploy on Netlify'
   (/admin/netlify_deploy_button/settings)
1. Enter the URL in the "Netlify URL page" tab and save
1. To trigger the deployment, go to the Netlify Deploy tab on the same page


## Maintainers

- Pratik Kamble - [pratik_kamble](https://www.drupal.org/u/pratik_kamble)
- [snehalgaikwad](https://www.drupal.org/u/snehalgaikwad)
