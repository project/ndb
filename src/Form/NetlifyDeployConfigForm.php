<?php

namespace Drupal\netlify_deploy_button\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form to get URL.
 */
class NetlifyDeployConfigForm extends ConfigFormBase {
  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'netlify_deploy_button.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'netlify_deploy_button_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['url'] = [
      '#type' => 'url',
      '#title' => t('Enter URL'),
      '#default_value' => $this->config('netlify_deploy_button.settings')->get('netlify_url'),
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => t('Submit'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config(self::SETTINGS)
      ->set('netlify_url', $form_state->getValue('url'))
      ->save();
    $this->messenger()->addStatus($this->t('URL has been updated.'));
  }

}
